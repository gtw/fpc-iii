EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 9
Title "FPC-III"
Date "2020-01-21"
Rev "1.0"
Comp "Gary Wong"
Comment1 "Lattice ECP5 FPGA board"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 4500 1500 1000 500 
U 5E11FD2E
F0 "HDMI" 50
F1 "hdmi.sch" 50
$EndSheet
$Sheet
S 1500 1500 1000 500 
U 5E18E657
F0 "Power" 50
F1 "power.sch" 50
$EndSheet
$Sheet
S 3000 1500 1000 500 
U 5E196FAD
F0 "Configuration" 50
F1 "config.sch" 50
$EndSheet
$Sheet
S 6000 1500 1000 500 
U 5E15624E
F0 "Ethernet" 50
F1 "ethernet.sch" 50
$EndSheet
$Sheet
S 7500 1500 1000 500 
U 5E16EC9B
F0 "RAM" 50
F1 "ram.sch" 50
$EndSheet
$Sheet
S 9000 1500 1000 500 
U 5E2D6BE4
F0 "USB" 50
F1 "usb.sch" 50
$EndSheet
$Sheet
S 1500 2500 1000 500 
U 5E215A8D
F0 "SRAM" 50
F1 "sram.sch" 50
$EndSheet
$Sheet
S 3000 2500 1000 500 
U 5E258A96
F0 "Radio" 50
F1 "radio.sch" 50
$EndSheet
$Comp
L Mechanical:MountingHole H2
U 1 1 5E33B991
P 600 7250
F 0 "H2" H 700 7296 50  0000 L CNN
F 1 "MountingHole" H 700 7205 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 600 7250 50  0001 C CNN
F 3 "~" H 600 7250 50  0001 C CNN
	1    600  7250
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5E33BAE0
P 600 7650
F 0 "H4" H 700 7696 50  0000 L CNN
F 1 "MountingHole" H 700 7605 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 600 7650 50  0001 C CNN
F 3 "~" H 600 7650 50  0001 C CNN
	1    600  7650
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5E33BBB6
P 600 7450
F 0 "H3" H 700 7496 50  0000 L CNN
F 1 "MountingHole" H 700 7405 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 600 7450 50  0001 C CNN
F 3 "~" H 600 7450 50  0001 C CNN
	1    600  7450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H1
U 1 1 5E33BC9F
P 600 7050
F 0 "H1" H 700 7096 50  0000 L CNN
F 1 "MountingHole" H 700 7005 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_DIN965" H 600 7050 50  0001 C CNN
F 3 "~" H 600 7050 50  0001 C CNN
	1    600  7050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
