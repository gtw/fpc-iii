//
// top.v
// Copyright 2020, Gary Wong <gtw@gnu.org>
//
// This file is part of FPC-III.
//
// FPC-III is a free hardware design: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// FPC-III is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with FPC-III.  If not, see <https://www.gnu.org/licenses/>.
	
module top( input clk_25,
	    output[ 7:0 ] led,
	    inout dp, inout dn,
	    output r, output g, output b, output c );

    reg clk;
    reg bitclk;
    
    EHXPLLL #( .CLKOP_DIV( 10 ), .CLKOS_DIV( 2 ), .FEEDBK_PATH( "CLKOS" ),
	       .CLKFB_DIV( 16 ) )
    pll( .CLKI( clk_25 ), .CLKOP( clk ), .CLKOS( bitclk ), .CLKFB( bitclk ) );

    wire[ 31:0 ] report;
    reg[ 7:0 ] modifiers;    
    reg[ 7:0 ] char0;
    wire interrupt;
    
    wishbone_usbkbd kbd( .dp( dp ), .dn( dn ), .interrupt( interrupt ),
			 .RST_I( 0 ), .CLK_I( clk ), .ADR_I( 1'b0 ),
			 .DAT_I( 32'h00000000 ), .DAT_O( report ),
			 .WE_I( 0 ), .SEL_I( 4'b0000 ), .STB_I( interrupt ),
			 .CYC_I( interrupt ) );
    
    assign led[ 7:0 ] = ~char0[ 7:0 ];

    always @( posedge clk )
	if( interrupt ) begin
	    modifiers[ 7:0 ] <= report[ 7:0 ];
	    char0[ 7:0 ] <= report[ 23:16 ];
	end

    wire[ 15:0 ] code;
    wire valid;
    screencode sc( modifiers, char0, code, valid );
    
    reg[ 9:0 ] cursor = 0;
    always @( posedge clk )
	if( interrupt && valid )
	    cursor <= cursor + 1;

    // not a valid Wishbone transaction, but the ways it's not compliant
    // are irrelevant for this particular slave
    wishbone_char char( .bitclk( bitclk ), .r( r ), .g( g ), .b( b ), .c( c ),
			.RST_I( 1'b0 ), .CLK_I( clk ),
			.ADR_I( { 19'b0, { cursor[ 9:6 ], 1'b0, cursor[ 5:1 ] }
				  + 11'h309 } ),
			.DAT_I( { code, code } ), .WE_I( 1'b1 ),
			.SEL_I( cursor[ 0 ] ? 4'b1100 : 4'b0011 ),
			.STB_I( valid ), .CYC_I( valid ) );
endmodule

module screencode( input[ 7:0 ] modifiers, input[ 7:0 ] char,
		   output reg[ 15:0 ] code, output valid );
    always @* begin
	if( modifiers & 8'h22 ) begin
	    // shifted
	    if( char < 30 )
		code[ 7:0 ] = 8'h40 | ( char - 3 ); // A..Z
	    else if( char == 31 )
		code[ 7:0 ] = 8'h40; // @
	    else if( char < 35 )
		code[ 7:0 ] = 8'h20 | ( char - 29 ); // !..%
	    else if( char == 35 )
		code[ 7:0 ] = 8'h5E;
	    else if( char == 36 )
		code[ 7:0 ] = 8'h26;
	    else if( char == 37 )
		code[ 7:0 ] = 8'h2A;
	    else if( char == 38 )
		code[ 7:0 ] = 8'h28;
	    else if( char == 39 )
		code[ 7:0 ] = 8'h29;
	    else if( char == 45 )
		code[ 7:0 ] = 8'h5F;
	    else if( char == 46 )
		code[ 7:0 ] = 8'h2B;
	    else if( char == 47 )
		code[ 7:0 ] = 8'h7B;
	    else if( char == 48 )
		code[ 7:0 ] = 8'h7D;
	    else if( char == 49 )
		code[ 7:0 ] = 8'h7C;
	    else if( char == 51 )
		code[ 7:0 ] = 8'h3A;
	    else if( char == 52 )
		code[ 7:0 ] = 8'h22;
	    else if( char == 53 )
		code[ 7:0 ] = 8'h7E;
	    else if( char == 54 )
		code[ 7:0 ] = 8'h3C;
	    else if( char == 55 )
		code[ 7:0 ] = 8'h3E;
	    else if( char == 56 )
		code[ 7:0 ] = 8'h3F;
	    else
		code[ 7:0 ] = 8'h20;
	end else begin
	    // unshifted
	    if( char < 30 )
		code[ 7:0 ] = 8'h60 | ( char - 3 ); // a..z
	    else if( char == 39 )
		code[ 7:0 ] = 8'h30; // 0
	    else if( char < 40 )
		code[ 7:0 ] = 8'h30 | ( char - 29 ); // !..)
	    else if( char == 45 )
		code[ 7:0 ] = 8'h2D;
	    else if( char == 46 )
		code[ 7:0 ] = 8'h3D;
	    else if( char == 47 )
		code[ 7:0 ] = 8'h5B;
	    else if( char == 48 )
		code[ 7:0 ] = 8'h5D;
	    else if( char == 49 )
		code[ 7:0 ] = 8'h5C;
	    else if( char == 51 )
		code[ 7:0 ] = 8'h3B;
	    else if( char == 52 )
		code[ 7:0 ] = 8'h27;
	    else if( char == 53 )
		code[ 7:0 ] = 8'h60;
	    else if( char == 54 )
		code[ 7:0 ] = 8'h2C;
	    else if( char == 55 )
		code[ 7:0 ] = 8'h2E;
	    else if( char == 56 )
		code[ 7:0 ] = 8'h2F;
	    else
		code[ 7:0 ] = 8'h20;
	end

	code[ 15:8 ] = ( modifiers & 8'h11 ? 0 : 1 ) |
		       ( modifiers & 8'h44 ? 0 : 2 ) |
		       ( modifiers & 8'h88 ? 0 : 4 );
    end
    
    assign valid = char >= 8'd4 && char <= 8'd56;
endmodule
